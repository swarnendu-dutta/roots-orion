<?php
/*===========================================================*/
/*    Theme Initialization
 /*===========================================================*/

if(!defined('THEME_URL')) {
  define('THEME_URL', get_template_directory_uri());
}

if(!defined('THEME_INCLUDES')) {
  define('THEME_INCLUDES', get_template_directory() . '/includes' );
}

if(!defined('SINGLE_PATH')) {
  define('SINGLE_PATH', get_template_directory() . '/single');
}

if(!defined('LAYOUTS_PATH')) {
  define('LAYOUTS_PATH', get_template_directory() . '/layouts');
}

register_nav_menu( 'primary-menu', __('Primary Menu', 'Orion'));
register_nav_menu( 'footer-menu', __('Footer Menu', 'Orion'));

include_once( THEME_INCLUDES .'/lib/advanced-custom-fields/acf.php');
include_once( THEME_INCLUDES .'/lib/acf-options-page/acf-options-page.php');
include_once( THEME_INCLUDES .'/lib/acf-repeater/acf-repeater.php');
include_once( THEME_INCLUDES .'/lib/acf-gallery/acf-gallery.php');
include_once( THEME_INCLUDES .'/acf-config.php');

//-----------Custom Post Types Initialization--------------//
include_once( THEME_INCLUDES . '/custom-post-types/events.php');
include_once( THEME_INCLUDES . '/custom-post-types/clients.php');
include_once( THEME_INCLUDES . '/custom-post-types/portfolios-taxonomy.php');
include_once( THEME_INCLUDES . '/custom-post-types/properties.php');
include_once( THEME_INCLUDES . '/custom-post-types/properties-taxonomy.php');
include_once( THEME_INCLUDES . '/custom-post-types/services.php');

//-----------Custom Fields Initialization--------------//
include_once( THEME_INCLUDES . '/custom-fields/careers.php');
include_once( THEME_INCLUDES . '/custom-fields/events.php');
include_once( THEME_INCLUDES . '/custom-fields/clients.php');
include_once( THEME_INCLUDES . '/custom-fields/properties.php');

/*===========================================================*/
/*    Setting Up Image Sizes
/*===========================================================*/

add_theme_support( 'post-thumbnails' );

/*-- Event Page --*/
add_image_size( 'event-main', 345, 488, true );
add_image_size( 'event-gallery-thumb', 284, 350, true );

/*===========================================================*/
/*    Loading Scripts
 /*===========================================================*/

function enqueue_orion_scripts() {
  wp_enqueue_script(
    'bootstrap-js',
    '//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js',
    array('jquery'),
    false
  ); // all the Bootstrap javascript goodness
  wp_enqueue_script(
    'bootstrapcdn',
    '//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js',
     false
  );
}

add_action('wp_enqueue_scripts', 'enqueue_orion_scripts');

function enqueue_orion_styles() {
  wp_enqueue_style(
    'bootstrap-style',
    '//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'
  );

  wp_enqueue_style(
    'cdn-bootstrap-style',
    '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'
  );

  wp_enqueue_style(
    'font-style',
    '//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'
  );
  wp_enqueue_style(
    'font-style',
    '//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'
  );
  wp_enqueue_style(
    'cdn-font-style',
    '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'
  );

  wp_enqueue_style( 'orion-style', THEME_URL . '/css/style.css');

}
add_action('wp_enqueue_scripts', 'enqueue_orion_styles');


/**
* Filter the single_template with our custom function
*/
add_filter('single_template', 'orion_single_template');

/**
* Single template function which will choose our template
*/
function orion_single_template($single) {
  global $wp_query, $post;
  /**
  * Checks for single template by ID
  */
  if(file_exists(SINGLE_PATH . '/single-' . $post->ID . '.php'))
    return SINGLE_PATH . '/single-' . $post->ID . '.php';

  /**
  * Checks for single template by category
  * Check by category slug and ID
  */
  foreach((array)get_the_category() as $cat) :

    if(file_exists(SINGLE_PATH . '/single-cat-' . $cat->slug . '.php'))
      return SINGLE_PATH . '/single-cat-' . $cat->slug . '.php';

    elseif(file_exists(SINGLE_PATH . '/single-cat-' . $cat->term_id . '.php'))
      return SINGLE_PATH . '/single-cat-' . $cat->term_id . '.php';

  endforeach;

  /**
  * Checks for single template by tag
  * Check by tag slug and ID
  */
  $wp_query->in_the_loop = true;
  foreach((array)get_the_tags() as $tag) :

    if(file_exists(SINGLE_PATH . '/single-tag-' . $tag->slug . '.php'))
      return SINGLE_PATH . '/single-tag-' . $tag->slug . '.php';

    elseif(file_exists(SINGLE_PATH . '/single-tag-' . $tag->term_id . '.php'))
      return SINGLE_PATH . '/single-tag-' . $tag->term_id . '.php';

  endforeach;
  $wp_query->in_the_loop = false;

  /**
  * Checks for single template by author
  * Check by user nicename and ID
  */
  $curauth = get_userdata($wp_query->post->post_author);

  if(file_exists(SINGLE_PATH . '/single-author-' . $curauth->user_nicename . '.php'))
    return SINGLE_PATH . '/single-author-' . $curauth->user_nicename . '.php';

  elseif(file_exists(SINGLE_PATH . '/single-author-' . $curauth->ID . '.php'))
    return SINGLE_PATH  . '/single-author-' . $curauth->ID . '.php';

  /**
  * Checks for single template by post type
  */
  if(file_exists(SINGLE_PATH. '/single-' . $post->post_type . '.php'))
    return SINGLE_PATH . '/single-' . $post->post_type . '.php';

  /**
  * Checks for default single post files within the single folder
  */
  if(file_exists(SINGLE_PATH . '/single.php'))
    return SINGLE_PATH . '/single.php';

  elseif(file_exists(SINGLE_PATH . '/default.php'))
    return SINGLE_PATH . '/default.php';

  return $single;
}
