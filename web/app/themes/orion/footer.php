<footer>
  <div class="container">
    <div class="row">
      <div class="col-xs-5 col-sm-2 social-icons">
        <a href="https://www.facebook.com">
          <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
          </span>
        </a>
        <a href="https://www.twitter.com">
          <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
          </span>
        </a>
        <a href="https://www.instagram.com">
          <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
          </span>
        </a>
      </div>
      <div class="col-xs-offset-1 col-sm-offset-6 col-md-offset-7 col-xs-6 col-sm-4 col-md-3 text-right">
        &copy;Copyright 2016 Orion
      </div>
    </div>
  </div>
</footer>
<script src="<?php echo THEME_URL ?>/js/main1.js"></script>
<?php wp_footer(); ?>
</body>
</html>
