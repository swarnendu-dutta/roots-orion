<?php
$category = get_the_category();
$event_category = get_field('event_category');
if($event_category) {
  $sub_menu_item_id = $event_category[0]->ID;
}
?>

<aside class="text-right col-xs-12 col-sm-3 col-ms-3 col-lg-3 services">
<ul>
  <li>
    <h2 class="text-uppercase">
      <a href="#">Services</a>
    </h2>
    <?php
    $args = array(
      'post_type' => 'service',
      'order' => 'ASC',
      'cat' => 2,
    );
    $the_query = new WP_Query( $args );
    if( $the_query->have_posts() ) :?>
      <ul class="current-sub-tabs">
        <?php while ($the_query->have_posts()) : $the_query->the_post();?>
          <li><h4 class="text-uppercase <?php print (get_the_ID() == $sub_menu_item_id)?
            'current-tab': '' ?>"><a class="<?php print (get_the_ID() == $sub_menu_item_id)?
            'aside-active': '' ?>" href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4></li>
        <?php endwhile ?>
      </ul>
    <?php endif ?>
  </li>

  <li>
    <h2 class="text-uppercase">
      <a href="#">portfolio</a>
    </h2>
    <ul class="">
      <?php
      $portfolio_terms = get_terms ( 'Industry' );
      if($portfolio_terms) : ?>
        <li>
          <h3 class="text-uppercase ">
            <a class="pink-text" href="#">Industry</a>
          </h3>
          <ul class="current-sub-tabs text-right">
            <?php foreach ($portfolio_terms as $industry) : ?>
              <li>
                <h4 class="text-uppercase">
                  <a class="aside-active" href="#"><?php print $industry->name ?></a>
                </h4>
              </li>
            <?php endforeach ?>
          </ul>
        </li>
      <?php endif ?>
      <li><h3 class="text-uppercase"><a class="sea-blue-text" href="#">clients</a></h3>
        <?php
        $args = array(
          'post_type' => 'client',
          'order' => 'ASC',
          'cat' => 2,
        );
        $client_query = new WP_Query( $args );
        if( $client_query->have_posts() ) :?>
          <ul class="text-right">
            <?php while ($client_query->have_posts()) : $client_query->the_post();?>
              <li>
                <h4 class="text-uppercase">
                  <a class="<?php print (get_the_ID() == $sub_menu_item_id)?
                  'aside-active': '' ?>" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                </h4>
              </li>
            <?php endwhile ?>
          </ul>
        <?php endif ?>
      </li>
    </ul>
  </li>
</ul>
</aside>
