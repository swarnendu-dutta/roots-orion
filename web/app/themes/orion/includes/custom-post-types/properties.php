<?php
add_action( 'init', 'register_cpt_property' );

function register_cpt_property() {
  $labels = array(
    'name' => __( 'properties', 'property' ),
    'singular_name' => __( 'property', 'property' ),
    'add_new' => __( 'Add New', 'property' ),
    'add_new_item' => __( 'Add New property', 'property' ),
    'new_item' => __( 'New property', 'property' ),
    'view_item' => __( 'View property', 'property' ),
    'search_items' => __( 'Search properties', 'property' ),
    'not_found' => __( 'No properties found', 'property' ),
    'not_found_in_trash' => __( 'No properties found in Trash', 'property' ),
    'parent_item_colon' => __( 'Parent property:', 'property' ),
    'menu_name' => __( 'Properties', 'property' ),
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'supports' => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies' => array( 'type' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => true,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
  );
  register_post_type( 'property', $args );
}
