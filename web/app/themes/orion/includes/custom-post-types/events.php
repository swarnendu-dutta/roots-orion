<?php
add_action( 'init', 'register_cpt_event' );

function register_cpt_event() {
  $labels = array(
    'name' => __( 'events', 'event' ),
    'singular_name' => __( 'event', 'event' ),
    'add_new' => __( 'Add New', 'event' ),
    'add_new_item' => __( 'Add New event', 'event' ),
    'new_item' => __( 'New event', 'event' ),
    'view_item' => __( 'View event', 'event' ),
    'search_items' => __( 'Search events', 'event' ),
    'not_found' => __( 'No events found', 'event' ),
    'not_found_in_trash' => __( 'No events found in Trash', 'event' ),
    'parent_item_colon' => __( 'Parent event:', 'event' ),
    'menu_name' => __( 'Events', 'event' ),
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'supports' => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies' => array( 'category', 'post_tag' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => true,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
  );
  register_post_type( 'event', $args );
}
