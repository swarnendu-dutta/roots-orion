<?php
// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_portfolios_taxonomies', 0 );

// create two taxonomies, Industrys and writers for the post Industry portfolio
function create_portfolios_taxonomies() {
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name'              => _x( 'Industry', 'taxonomy general name' ),
    'singular_name'     => _x( 'Industry', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Industries' ),
    'all_items'         => __( 'All Industries' ),
    'parent_item'       => __( 'Parent Industry' ),
    'parent_item_colon' => __( 'Parent Industry:' ),
    'edit_item'         => __( 'Edit Industry' ),
    'update_item'       => __( 'Update Industry' ),
    'add_new_item'      => __( 'Add New Industry' ),
    'new_item_name'     => __( 'New Industry Name' ),
    'menu_name'         => __( 'Industry' ),
  );

  $args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'industry' ),
  );

  register_taxonomy( 'Industry', array( 'client' ), $args );
}
