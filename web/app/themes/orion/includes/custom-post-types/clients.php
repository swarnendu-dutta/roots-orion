<?php
add_action( 'init', 'register_cpt_client' );

function register_cpt_client() {
  $labels = array(
    'name' => __( 'clients', 'client' ),
    'singular_name' => __( 'client', 'client' ),
    'add_new' => __( 'Add New', 'client' ),
    'add_new_item' => __( 'Add New client', 'client' ),
    'new_item' => __( 'New client', 'client' ),
    'view_item' => __( 'View client', 'client' ),
    'search_items' => __( 'Search clients', 'client' ),
    'not_found' => __( 'No clients found', 'client' ),
    'not_found_in_trash' => __( 'No clients found in Trash', 'client' ),
    'parent_item_colon' => __( 'Parent client:', 'client' ),
    'menu_name' => __( 'Clients', 'client' ),
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'supports' => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies' => array( 'category', 'post_tag' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => true,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
  );
  register_post_type( 'client', $args );
}
