<?php
add_action( 'init', 'register_cpt_service' );

function register_cpt_service() {
  $labels = array(
    'name' => __( 'Services', 'service' ),
    'singular_name' => __( 'service', 'service' ),
    'add_new' => __( 'Add New', 'service' ),
    'add_new_item' => __( 'Add New service', 'service' ),
    'new_item' => __( 'New service', 'service' ),
    'view_item' => __( 'View service', 'service' ),
    'search_items' => __( 'Search services', 'service' ),
    'not_found' => __( 'No services found', 'service' ),
    'not_found_in_trash' => __( 'No services found in Trash', 'service' ),
    'parent_item_colon' => __( 'Parent service:', 'service' ),
    'menu_name' => __( 'Services', 'service' ),
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'supports' => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies' => array( 'category', 'post_tag' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => true,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
  );
  register_post_type( 'service', $args );
}
