<?php
add_action( 'init', 'register_cpt_portfolio' );

function register_cpt_portfolio() {
  $labels = array(
    'name' => __( 'portfolios', 'portfolio' ),
    'singular_name' => __( 'portfolio', 'portfolio' ),
    'add_new' => __( 'Add New', 'portfolio' ),
    'add_new_item' => __( 'Add New portfolio', 'portfolio' ),
    'new_item' => __( 'New portfolio', 'portfolio' ),
    'view_item' => __( 'View portfolio', 'portfolio' ),
    'search_items' => __( 'Search portfolios', 'portfolio' ),
    'not_found' => __( 'No portfolios found', 'portfolio' ),
    'not_found_in_trash' => __( 'No portfolios found in Trash', 'portfolio' ),
    'parent_item_colon' => __( 'Parent portfolio:', 'portfolio' ),
    'menu_name' => __( 'Portfolios', 'portfolio' ),
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'supports' => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies' => array( 'industry' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => true,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
  );
  register_post_type( 'portfolio', $args );
}
