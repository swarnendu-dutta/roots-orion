<?php
if(function_exists("register_field_group"))
{
  register_field_group(array (
    'id' => 'acf_events',
    'title' => 'Events',
    'fields' => array (
      array (
        'key' => 'field_56f54da05dbe7',
        'label' => 'Event Thumb',
        'name' => 'event_thumb',
        'type' => 'image',
        'required' => 1,
        'save_format' => 'url',
        'preview_size' => 'thumbnail',
        'library' => 'all',
      ),
      array (
        'key' => 'field_56f550b8ca39c',
        'label' => 'Event Category',
        'name' => 'event_category',
        'type' => 'relationship',
        'return_format' => 'object',
        'post_type' => array (
          0 => 'service',
        ),
        'taxonomy' => array (
          0 => 'category:2',
        ),
        'filters' => array (
          0 => 'search',
        ),
        'result_elements' => array (
          0 => 'post_type',
          1 => 'post_title',
        ),
        'max' => '',
      ),
      array (
        'key' => 'field_56f54fca5dbe8',
        'label' => 'Event Location',
        'name' => 'event_location',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_56f54fd55dbe9',
        'label' => 'Event Date',
        'name' => 'event_date',
        'type' => 'date_picker',
        'date_format' => 'yymmdd',
        'display_format' => 'dd/mm/yy',
        'first_day' => 1,
      ),
      array (
        'key' => 'field_56f54fea5dbea',
        'label' => 'Event Videos',
        'name' => 'event_videos',
        'type' => 'repeater',
        'sub_fields' => array (
          array (
            'key' => 'field_56f5501b5dbeb',
            'label' => 'Event Video',
            'name' => 'event_video',
            'type' => 'text',
            'column_width' => '',
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'html',
            'maxlength' => '',
          ),
          array (
            'key' => 'field_56f550375dbec',
            'label' => 'Event Video Description',
            'name' => 'event_video_description',
            'type' => 'wysiwyg',
            'column_width' => '',
            'default_value' => '',
            'toolbar' => 'full',
            'media_upload' => 'yes',
          ),
        ),
        'row_min' => '',
        'row_limit' => '',
        'layout' => 'table',
        'button_label' => 'Add Video',
      ),
      array (
        'key' => 'field_56f550495dbed',
        'label' => 'Event Photo Gallery',
        'name' => 'event_photo_gallery',
        'type' => 'gallery',
        'preview_size' => 'thumbnail',
        'library' => 'all',
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'event',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array (
      'position' => 'normal',
      'layout' => 'no_box',
      'hide_on_screen' => array (
      ),
    ),
    'menu_order' => 0,
  ));
}
