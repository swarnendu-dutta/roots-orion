<?php
if(function_exists("register_field_group"))
{
  register_field_group(array (
    'id' => 'acf_clients',
    'title' => 'Clients',
    'fields' => array (
      array (
        'key' => 'field_56f552a25b95c',
        'label' => 'Client Thumb',
        'name' => 'client_thumb',
        'type' => 'image',
        'save_format' => 'url',
        'preview_size' => 'thumbnail',
        'library' => 'all',
      ),
      array (
        'key' => 'field_56f552ca5b95d',
        'label' => 'Client Location',
        'name' => 'client_location',
        'type' => 'text',
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_56f5554d5b960',
        'label' => 'Services',
        'name' => 'services',
        'type' => 'repeater',
        'sub_fields' => array (
          array (
            'key' => 'field_56f555dd5b961',
            'label' => 'Services Category',
            'name' => 'services category',
            'type' => 'relationship',
            'column_width' => '',
            'return_format' => 'object',
            'post_type' => array (
              0 => 'service',
            ),
            'taxonomy' => array (
              0 => 'category:3',
              1 => 'category:2',
              2 => 'category:2',
              3 => 'category:2',
              4 => 'category:2',
              5 => 'category:2',
              6 => 'category:2',
            ),
            'filters' => array (
              0 => 'search',
            ),
            'result_elements' => array (
              0 => 'post_type',
              1 => 'post_title',
            ),
            'max' => '',
          ),
          array (
            'key' => 'field_56f5568f5b962',
            'label' => 'Client Service Main Image',
            'name' => 'client_service_main_image',
            'type' => 'image',
            'column_width' => '',
            'save_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
          ),
          array (
            'key' => 'field_56f556de5b963',
            'label' => 'Client Service Description',
            'name' => 'client_service_description',
            'type' => 'wysiwyg',
            'column_width' => '',
            'default_value' => '',
            'toolbar' => 'full',
            'media_upload' => 'yes',
          ),
          array (
            'key' => 'field_56f556ee5b964',
            'label' => 'Client Service Gallery',
            'name' => 'client_service_gallery',
            'type' => 'gallery',
            'column_width' => '',
            'preview_size' => 'thumbnail',
            'library' => 'all',
          ),
        ),
        'row_min' => '',
        'row_limit' => '',
        'layout' => 'row',
        'button_label' => 'Add Service Details',
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'client',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array (
      'position' => 'normal',
      'layout' => 'no_box',
      'hide_on_screen' => array (
        0 => 'custom_fields',
      ),
    ),
    'menu_order' => 0,
  ));
}
