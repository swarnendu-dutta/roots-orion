<?php
if(function_exists("register_field_group"))
{
  register_field_group(array (
    'id' => 'acf_properties',
    'title' => 'Properties',
    'fields' => array (
      array (
        'key' => 'field_56f55c8296ec7',
        'label' => 'Properties Gallery',
        'name' => 'properties_gallery',
        'type' => 'gallery',
        'preview_size' => 'thumbnail',
        'library' => 'all',
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'property',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array (
      'position' => 'normal',
      'layout' => 'no_box',
      'hide_on_screen' => array (
      ),
    ),
    'menu_order' => 0,
  ));
}
