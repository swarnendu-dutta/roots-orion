<?php
if(function_exists("register_field_group"))
{
  register_field_group(array (
    'id' => 'acf_careers',
    'title' => 'Careers',
    'fields' => array (
      array (
        'key' => 'field_56f4e46e31796',
        'label' => 'Career Section',
        'name' => 'career_section',
        'type' => 'repeater',
        'sub_fields' => array (
          array (
            'key' => 'field_56f4e4a831797',
            'label' => 'Domain',
            'name' => 'domain',
            'type' => 'text',
            'column_width' => '',
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'formatting' => 'html',
            'maxlength' => '',
          ),
          array (
            'key' => 'field_56f4e4c231798',
            'label' => 'Profile Section',
            'name' => 'profile_section',
            'type' => 'repeater',
            'column_width' => '',
            'sub_fields' => array (
              array (
                'key' => 'field_56f4e4dc31799',
                'label' => 'Job Profile',
                'name' => 'job_profile',
                'type' => 'text',
                'column_width' => '',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
              ),
              array (
                'key' => 'field_56f4e4ec3179a',
                'label' => 'Job Description',
                'name' => 'job_description',
                'type' => 'wysiwyg',
                'column_width' => '',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
              ),
              array (
                'key' => 'field_56f4e4fc3179b',
                'label' => 'Minimum Requirements',
                'name' => 'minimum_requirements',
                'type' => 'wysiwyg',
                'column_width' => '',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
              ),
              array (
                'key' => 'field_56f4e5153179c',
                'label' => 'Career Form',
                'name' => 'career_form',
                'type' => 'wysiwyg',
                'column_width' => '',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'yes',
              ),
            ),
            'row_min' => '',
            'row_limit' => '',
            'layout' => 'row',
            'button_label' => 'Add Profile',
          ),
        ),
        'row_min' => '',
        'row_limit' => '',
        'layout' => 'table',
        'button_label' => 'Add Career Section',
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'page_template',
          'operator' => '==',
          'value' => 'templates/template-careers.php',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array (
      'position' => 'normal',
      'layout' => 'no_box',
      'hide_on_screen' => array (
      ),
    ),
    'menu_order' => 0,
  ));
}
