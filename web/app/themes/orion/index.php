<?php
get_header();
?>

<section id="content">
<?php if ( have_posts() ) : ?>

<?php /* Start the Loop */ ?>
<?php while ( have_posts() ) : the_post(); ?>
<h2><?php the_title(); ?></h2>
<?php the_content(); ?>
<?php endwhile; ?>

<?php endif; ?>
</section>
<?php get_footer(); ?>
