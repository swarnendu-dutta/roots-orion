<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title('|', true, 'right' ) ?><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
    <link href='https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
  </head>
  <body <?php body_class(); ?>>
    <header  class="container">
      <div class="row">
        <div class="logo col-xs-6 col-sm-2 col-md-2">
          <img src="<?php echo THEME_URL ?>/img/logo.png" class="img-responsive" alt="LOGO" />
        </div>
        <div class="navbar col-sm-offset-5 col-sm-5 col-md-offset-6 col-md-4 col-lg-offset-7 col-lg-3">
          <!--Brand and toggle grouped for mobile display-->
          <div class="navbar-header">
            <button type="button" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false" class="navbar-toggle collapsed">
              <span class="sr-only">Toggle Navigation</span>
              <img src="<?php echo THEME_URL ?>/img/menu.jpg" alt="<>" />
            </button>
          </div>
          <!--nav bar for toggling-->
          <div id="main-navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="col-sm-6 col-md-6"><a href="#" class="active">HOME</a></li>
                <li class="col-sm-6 col-md-6"><a href="#">ABOUT</a></li>
                <li class="col-sm-6 col-md-6"><a href="#">MARKETING</a></li>
                <li class="col-sm-6 col-md-6"><a href="#">DESIGN</a></li>
                <li class="col-sm-6 col-md-6"><a href="#">PROPERTIES</a></li>
                <li class="col-sm-6 col-md-6"><a href="#">BLOG</a></li>
                <li class="col-sm-6 col-md-6"><a href="#">CAREERS</a></li>
                <li class="col-sm-6 col-md-6"><a href="#">CONTACT</a></li>
            </ul>
          </div>
      </div>
    </header>
