<?php
/* Template Name: Single Events Page */
get_header();
?>
<?php
$event_category = get_field('event_category');
$post_name = str_replace('-', ' ', $event_category[0]->post_name);
$post_type_name = $GLOBALS['wp_post_types'][ $event_category[0]->post_type ]->labels->name;
?>

<section class="container-fluid services">
  <div class="page-header">
    <div class="row">
      <div class="col-md-5"><img class="img-responsive header-design-element"
      src="<?php echo THEME_URL ?>/img/header-design-element.png" alt="" /></div>
      <div class="col-md-2"><h2 class="text-uppercase yellow-text text-center">
      <?php the_category(' '); ?></h2></div>
      <div class="col-md-5"><img class="img-responsive header-design-element"
      src="<?php echo THEME_URL ?>/img/header-design-element.png" alt="" /></div>
    </div>
  </div>

  <div class="row">
    <?php get_sidebar() ?>
    <?php while ( have_posts() ) : the_post(); ?>
    <div class="contents col-xs-12 col-sm-9 col-ms-9 col-lg-9">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <ol class="breadcrumb">
            <li><a href="#"><?php the_category(' '); ?></a></li>
            <li><a href="#"><?php print $post_type_name ?></a></li>
            <li><?php print $post_name ?></li>
          </ol>
        </div>
      </div>
      <div class="row btm-padding-40px">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
          <div class="text-right text-uppercase">
            <h2 class="yellow-text"><?php print get_the_title() ?></h2>
            <h3>Pune Launch</h3>
            <h4 class="text-muted"><?php print date('js M Y', strtotime(get_field('event_date')))?></h4>
            <h4 class="text-muted"><?php print get_field('event_location')?></h4>
            <div class="right-rectangle yellow-bg"></div>
          </div>
          <div class="text-right">
            <p>
              <?php the_content(); ?>
            </p>
          </div>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
          <!--<img class="img-responsive" src="<?php echo THEME_URL ?>/img/services/Indievent-page_1.png" alt="" />-->
          <?php print the_post_thumbnail('event-main', array('class' => 'img-responsive'))?>
        </div>
      </div>
      <div class="services-design-element">
        <img class="img-responsive pull-right" src="<?php echo THEME_URL ?>/img/home-side-des3.png" alt="" />
      </div>
      <div class="row btm-padding-40px">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <h3 class="yellow-text text-uppercase">Event video</h3>
          <div class="left-rectangle yellow-bg"></div>
        </div>
        <?php
        if( have_rows('event_videos') ):
          while( have_rows('event_videos') ) : the_row();
            ?>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
              <?php print get_sub_field('event_video') ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
              <h3 class="yellow-text text-uppercase">Key stats</h3>
              <p>
                <?php print get_sub_field('event_video_description') ?>
              </p>
            </div>
          <?php
          endwhile;
        endif;
        ?>
      </div>

      <div class="row btm-padding-40px">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <h3 class="yellow-text text-uppercase">Photo Gallery</h3>
          <div class="left-rectangle yellow-bg"></div>
        </div>
        <?php
        $event_images = get_field('event_photo_gallery');
        if( $event_images ):
          $gallery_grid_img_count = 1;
          foreach( $event_images as $image ):
          ?>
          <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <a href="<?php echo $image['sizes']['large']; ?>" rel="lightbox[gallery]">
              <img class="attachment-thumbnail img-responsive thickbox"
              src="<?php echo $image['sizes']['event-gallery-thumb']; ?>" alt="" />
            </a>
          </div>
          <?php
          if($gallery_grid_img_count % 3 == 0)
            print '<div class="clearfix"></div>';
          ?>
          <?php $gallery_grid_img_count++ ?>
          <?php endforeach; ?>
        <?php endif ?>
      </div>

    </div>
    <?php endwhile ?>
  </div>

</section>

<?php get_footer(); ?>
