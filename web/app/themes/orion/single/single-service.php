<?php
/* Template Name: Single Services Page */
get_header();
?>

<?php
$event_category = get_field('event_category');
$post_name = str_replace('-', ' ', $event_category[0]->post_name);
$post_type_name = $GLOBALS['wp_post_types'][ $event_category[0]->post_type ]->labels->name;
?>
<?php while ( have_posts() ) : the_post(); ?>
<section class="container-fluid services">
  <div class="page-header">
    <div class="row">
      <div class="col-md-5"><img class="img-responsive header-design-element"
      src="<?php echo THEME_URL ?>/img/header-design-element.png" alt="" /></div>
      <div class="col-md-2">
        <h2 class="text-uppercase yellow-text text-center">
          <?php the_category(' '); ?>
        </h2>
      </div>
      <div class="col-md-5"><img class="img-responsive header-design-element"
      src="<?php echo THEME_URL ?>/img/header-design-element.png" alt="" /></div>
    </div>
  </div>

  <div class="row">
    <?php get_sidebar() ?>

    <div class="contents col-xs-12 col-sm-9 col-ms-9 col-lg-9">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <ol class="breadcrumb">
            <li><a href="#"><?php the_category(' '); ?></a></li>
            <li><a href="#"><?php print $post_type_name ?></a></li>
            <li><?php print get_the_title() ?></li>
          </ol>
        </div>
      </div>
      <div class="row btm-padding-40px">
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
          <img class="img-responsive" src="<?php echo THEME_URL ?>/img/home2.jpg" alt="" />
        </div>
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
          <h2 class="text-uppercase">Lorem ipsum dolor sit amet</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 pull-right">
          <img src="<?php echo THEME_URL ?>/img/services/services-design-element.png" alt="" />
        </div>
      </div>

      <div class="row btm-padding-40px">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <h3 class="yellow-text text-uppercase">All events</h3>
        </div>

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <img class="img-responsive" src="<?php echo THEME_URL ?>/img/services/services05.jpg" alt="" />
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <img class="img-responsive" src="<?php echo THEME_URL ?>/img/services/services06.jpg" alt="" />
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <img class="img-responsive" src="<?php echo THEME_URL ?>/img/services/services05.jpg" alt="" />
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <img class="img-responsive" src="<?php echo THEME_URL ?>/img/services/services06.jpg" alt="" />
        </div>

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <img class="img-responsive" src="<?php echo THEME_URL ?>/img/services/services05.jpg" alt="" />
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <img class="img-responsive" src="<?php echo THEME_URL ?>/img/services/services06.jpg" alt="" />
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <img class="img-responsive" src="<?php echo THEME_URL ?>/img/services/services05.jpg" alt="" />
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <img class="img-responsive" src="<?php echo THEME_URL ?>/img/services/services06.jpg" alt="" />
        </div>

        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <img class="img-responsive" src="<?php echo THEME_URL ?>/img/services/services05.jpg" alt="" />
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <img class="img-responsive" src="<?php echo THEME_URL ?>/img/services/services06.jpg" alt="" />
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <img class="img-responsive" src="<?php echo THEME_URL ?>/img/services/services05.jpg" alt="" />
        </div>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
          <img class="img-responsive" src="<?php echo THEME_URL ?>/img/services/services06.jpg" alt="" />
        </div>

      </div>

    </div>
  </div>

</section>
<?php endwhile; ?>

<?php get_footer(); ?>
