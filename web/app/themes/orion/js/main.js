jQuery(function($) {
  $( window ).resize(function() {
    var windowWidth = $( window ).width();

    // calculates width and sets width to section-title for home page
    var homePageSectionDiv = 0.48;
    if ( windowWidth > 767 && windowWidth < 992){
      homePageSectionDiv = 0.47;
    }else if (windowWidth <=767 ) {
      homePageSectionDiv = 0.8;
    }
    remainingWidth = (windowWidth * homePageSectionDiv ) - $("section h1").width() ;
    $(".right-rectangle").width(remainingWidth);
    $(".left-rectangle").width(remainingWidth);
    // calculates width and sets width to section-title for home page: ends here

    // calculates height of section>img in homepage and sets height for black background
    var blackBgSectionHeight = $("section .container img.img-responsive").height() - $(".section-title").height() - 100;
    if ( windowWidth > 767 && windowWidth < 1200){
      blackBgSectionHeight = $("section .container .row .col-xs-12:last-of-type").height() + 50;
    }else if (windowWidth <=767 ) {
      blackBgSectionHeight = $("section .container img.img-responsive").height() + $("section .container .row .col-xs-12:last-of-type").height()
    }
    $(".black-bg-section").height(blackBgSectionHeight);
    // calculates height of section>img in homepage and sets height for black background: ends here
  });

  $(document).ready(function($){
    var windowWidth = $( window ).width();
    $(".nav a").on("click", function(){
       $(".nav").find(".active").removeClass("active");
       $(this).addClass("active");
    });

    // calculates width and sets width to section-title for home page
    var homePageSectionDiv = 0.48;
    if ( windowWidth > 767 && windowWidth < 992){
      homePageSectionDiv = 0.47;
    }else if (windowWidth <=767 ) {
      homePageSectionDiv = 0.8;
    }
    remainingWidth = (windowWidth * homePageSectionDiv ) - $("section h1").width() ;
    $(".right-rectangle").width(remainingWidth);
    $(".left-rectangle").width(remainingWidth);
    // calculates width and sets width to section-title for home page: ends here

    // calculates height of section>img in homepage and sets height for black background
    var blackBgSectionHeight = $("section .container img.img-responsive").height() - $(".section-title").height() - 100;

    if ( windowWidth > 767 && windowWidth < 1200){
      blackBgSectionHeight = $("section .container .row .col-xs-12:last-of-type").height() + 50;
    }else if (windowWidth <=767 ) {
      blackBgSectionHeight = $("section .container img.img-responsive").height() + $("section .container .row .col-xs-12:last-of-type").height()
    }
    $(".black-bg-section").height(blackBgSectionHeight);
    // calculates height of section>img in homepage and sets height for black background: ends here

  });//document ready enclosure

  // $.fn.windowWidth = function() {
  //     return $( window ).width();
  //  };


});//jquery enclosure
