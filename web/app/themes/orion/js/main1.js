jQuery(function($) {
  $( window ).resize(function() {
    var windowWidth = $( window ).width();

    // calculates width and sets width to section-title for home page
    var homePageSectionDiv = 0.5;
    if (windowWidth <=767 ) {
      homePageSectionDiv = 0.8;
    }

    $(".right-rectangle").width( (windowWidth * homePageSectionDiv ) - $(".right-rectangle").prev().width()-16 );
    $(".left-rectangle").width( (windowWidth * homePageSectionDiv ) - $(".left-rectangle").next().width()-16 );
    // calculates width and sets width to section-title for home page: ends here

    // calculates height of section>img in homepage and sets height for black background
    var blackBgSectionHeight = $("section .pic img.img-responsive").height() - $(".section-title").height() - 50;

    if ( windowWidth > 767 && windowWidth < 1200){
      blackBgSectionHeight = $("section .text").height() + 50;
    }else if (windowWidth <=767 ) {
      blackBgSectionHeight = $("section .pic img.img-responsive").height() + $("section .text").height()
    }
    $(".black-bg-section").height(blackBgSectionHeight);
    // calculates height of section>img in homepage and sets height for black background: ends here
  });

  $(document).ready(function($){
    var windowWidth = $( window ).width();
    $(".nav a").on("click", function(){
       $(".nav").find(".active").removeClass("active");
       $(this).addClass("active");
    });

    // calculates width and sets width to section-title for home page
    var homePageSectionDiv = 0.5;
    if (windowWidth <=767 ) {
      homePageSectionDiv = 0.8;
    }
    $(".right-rectangle").width( (windowWidth * homePageSectionDiv ) - $(".right-rectangle").prev().width()-16 );
    $(".left-rectangle").width( (windowWidth * homePageSectionDiv ) - $(".left-rectangle").next().width()-16 );
    // calculates width and sets width to section-title for home page: ends here

    // calculates height of section>img in homepage and sets height for black background
    var blackBgSectionHeight = $("section .pic img.img-responsive").height() - $(".section-title").height() - 50;

    if ( windowWidth > 767 && windowWidth < 1200){
      blackBgSectionHeight = $("section .text").height() + 50;
    }else if (windowWidth <=767 ) {
      blackBgSectionHeight = $("section .pic img.img-responsive").height() + $("section .text").height()
    }
    $(".black-bg-section").height(blackBgSectionHeight);
    // calculates height of section>img in homepage and sets height for black background: ends here

  });//document ready enclosure



});//jquery enclosure
